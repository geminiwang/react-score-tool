import React from "react";
import ReactDOM from "react-dom";
// import Router from "./Router/router";
import "antd/dist/antd.css";
import * as serviceWorker from "./serviceWorker";
import "./assets/css/layout.scss";
import "./assets/css/reset.scss";
import App from "./App";

window.onresize = setHtmlFontSize;

//适配用
function setHtmlFontSize() {
  const htmlWidth =
    document.documentElement.clientWidth || document.body.clientWidth;
  const htmlDom = document.getElementsByTagName("html")[0];
  htmlDom.style.fontSize = htmlWidth / 19.2 + "px";
}
setHtmlFontSize();

console.log(`Looks like we are in ${process.env.NODE_ENV} mode!`);

ReactDOM.render(<App />, document.getElementById("root"));

serviceWorker.unregister();
