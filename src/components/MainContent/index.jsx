import React from "react";
import { Redirect, withRouter, Route, Switch } from "react-router-dom";
// import { TransitionGroup, CSSTransition } from 'react-transition-group';
import { routes } from "../../Router/routes.js";
const MainContent = ({ location }) => {
  // const handleFilter = permission => {
  // 	// 过滤没有权限的页面
  // 	if (!permission || permission === roleType) return true;
  // 	return false;
  // };
  return (
    <div>
      <Switch>
        {routes.map(ele => (
          <Route
            render={() => <ele.component />}
            key={ele.path}
            path={ele.path}
          />
        ))}
        <Redirect from="/" exact to="/imgMgt" />
        <Redirect to="/error/404" />
      </Switch>
    </div>
  );
};
export default withRouter(MainContent);
