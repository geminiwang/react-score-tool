import * as actionTypes from "../constants/index";
const defaultState = {
  posts: []
};
const post_reducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.GET_POSTS:
      return {
        ...state,
        posts: action.data
      };
    default:
      return state;
  }
};

export default post_reducer;
