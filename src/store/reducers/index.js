import { combineReducers } from "redux";
import user from "./userInfo";
import post_reducer from "./post_reducer";
// import tagList from './tagList';
// import { breadCrumb, tags, theme, collapse } from './setting';
export default combineReducers({ user, post_reducer }); //, tagList, breadCrumb, tags, theme, collapse });
// Redux 提供了一个combineReducers方法，用于 Reducer 的拆分。你只要定义各个子 Reducer 函数，然后用这个方法，将它们合成一个大的 Reducer
