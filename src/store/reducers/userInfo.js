import * as actionTypes from "../constants/index";
const defaultState = {
  userInfo: {},
  token: "222222222222222222222"
};
const user = (state = defaultState, action) => {
  console.log("ACTION NOW IS" + JSON.stringify(action));
  // let newState = JSON.parse(JSON.stringify(state));
  switch (action.type) {
    case actionTypes.SET_USERINFO:
      // newState.userInfo = action.data;
      // return newState;
      return {
        ...state,
        userInfo: action.data
      };
    case actionTypes.SET_FAKETOKEN:
      // newState.token = action.data;
      // return newState;
      return {
        ...state,
        token: action.data
      };
    case actionTypes.SET_REALTOKEN:
      return action.data + "john snow";
    default:
      return state;
  }
};

export default user;
