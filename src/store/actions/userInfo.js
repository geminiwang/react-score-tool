import * as actionTypes from "../constants/index";
import axios from "axios";
const setUserInfo = data => {
  return {
    type: actionTypes.SET_USERINFO,
    data
  };
};

const setFakeToken = data => {
  return {
    type: actionTypes.SET_FAKETOKEN,
    data
  };
};

const setRealTOKEN = data => {
  return {
    type: actionTypes.SET_REALTOKEN,
    data
  };
};

const getData = function() {
  return axios.get("http://yapi.demo.qunar.com/mock/64880/api/cms/getPostData");
};

const getPosts = async dispatch => {
  const res = await getData();
  dispatch({
    type: "GET_POST",
    data: res.data.data
  });
};
export { setUserInfo, setFakeToken, setRealTOKEN, getPosts };
