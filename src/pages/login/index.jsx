import React, { Component } from "react";
import titleImg from "../../assets/img/logoText.svg";
import { Form, Icon, Input, Button, Checkbox } from "antd";
import axios from "axios";
import {
  setUserInfo,
  setFakeToken,
  getPosts
} from "../../store/actions/userInfo";
import { connect } from "react-redux";
import "./index.scss";
// import imgApi from "../../api/index";
import store from "../../store/store.js";
class Login extends Component {
  constructor(props) {
    super(props);
    console.log("Store Now:" + JSON.stringify(store.getState()));

    this.state = {};
  }
  submitFn(obj) {
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log(this.props);

        // let data = {
        //   userName: obj.username,
        //   userPsw: obj.password
        // };
        // imgApi.login(data).then(res => {
        //   if (res.data.respCode === 0) {
        //     localStorage.userId = res.data.data.userId;
        //     localStorage.userName = res.data.data.userName;
        //     localStorage.setItem('isLogin',1);
        //     this.props.history.push("/imgMgt");
        //     // return <Redirect to="/imgMgt" />;
        //   }
        // });
        //判断一下环境
        axios
          .get("http://yapi.demo.qunar.com/mock/64880/api/cms/getUserInfo")
          .then(res => {
            if (res.data.respCode === 0) {
              console.log(this.props);
              localStorage.userId = res.data.data.userId;
              localStorage.userName = res.data.data.userName;
              localStorage.setItem("isLogin", 1);
              this.props.setUserInfo(res.data.data);
              this.props.setFakeToken(res.data.token);
              this.props.getPosts();
              this.props.history.push("/imgMgt");
              // return <Redirect to="/imgMgt" />;
            }
          });
        // const tokenAction = setRealToken();
        // Store.dispatch();
      }
    });
  }
  render() {
    const { getFieldDecorator, getFieldsValue } = this.props.form;
    return (
      <div className="bg_box">
        <div className="login_box">
          <div className="login_box_title">
            <img src={titleImg} alt="" />
          </div>
          <div className="login_box_input">
            <Form onSubmit={this.handleSubmit} className="login-form">
              <Form.Item>
                {getFieldDecorator("username", {
                  rules: [{ required: true, message: "请输入用户名" }]
                })(
                  <Input
                    className="login_input"
                    prefix={
                      <Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    placeholder="Username"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("password", {
                  rules: [{ required: true, message: "请输入您的密码" }]
                })(
                  <Input
                    className="login_input second_input"
                    prefix={
                      <Icon type="lock" style={{ color: "rgba(0,0,0,.25)" }} />
                    }
                    type="password"
                    placeholder="Password"
                  />
                )}
              </Form.Item>
              <Form.Item>
                {getFieldDecorator("remember", {
                  valuePropName: "checked",
                  initialValue: true
                })(<Checkbox>Remember me</Checkbox>)}
                {/* <a className="login-form-forgot" href="">
                </a> */}
                {/* <Button
                  type="primary"
                  htmlType="submit"
                  className="login-form-button"
                >
                  Log in
                </Button> */}
              </Form.Item>
            </Form>
          </div>
          <div className="login_box_btn">
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.submitFn.bind(this, getFieldsValue())}
            >
              登录
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  userInfo: state.userInfo,
  token: state.token
});

/**
 * 此处也可以自行编写一套action
 *
 * @param {*} dispatch
 * let action={
 *  type:'SET_FAKETOKEN'
 *  value:data
 * }
 *
 * 然后直接dispatch 这个action 但是我们也可以引入已经写好的
 * action, 如下
 */
const mapDispatchToProps = dispatch => ({
  setUserInfo: data => {
    dispatch(setUserInfo(data));
  },
  setFakeToken: data => {
    dispatch(setFakeToken(data));
  },
  getPosts: () => {
    getPosts(dispatch);
  }
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Login));
