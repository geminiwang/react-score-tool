import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "antd";
import Nav from "../../components/Nav/index.jsx";
import Header from "../../components/Header/index.jsx";
import { setUserInfo } from "../../store/actions/userInfo";
import MainContent from "../../components/MainContent/index";

//此页为主框架

class Index extends Component {
  render() {
    // const { breadCrumb } = this.props;
    return (
      // <div className="layout">
      // 	<Layout style={{ minHeight: '100vh' }}>
      // 		<SideMenu />
      // 		<Layout>
      // 			<TopHeader />
      // 			{breadCrumb.show ? <BreadCrumb /> : null}
      // 			<MainContent />
      // 		</Layout>
      // 	</Layout>
      // </div>
      <Row className="base_warp">
        <Header />
        <Col span={3} className="nav_warp">
          {/* 侧面栏 */}
          <Nav></Nav>
        </Col>
        <Col span={21} className="main_warp">
          {/* {this.props.children} */}
          <MainContent />
          {/* 
            此处应该是router switch 而不是prop.children
           */}
        </Col>
        {/* Footer */}
      </Row>
    );
  }
}
const mapStateToProps = state => state; //Get
const mapDispatchToProps = dispatch => ({
  //Set
  setUserInfo: data => {
    dispatch(setUserInfo(data));
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Index);
