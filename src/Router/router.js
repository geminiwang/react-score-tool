import React from "react";
import { Router, Route, Switch } from "react-router-dom";
// import imgMgt from "../pages/imgMgt/index.jsx";
// import textMgt from "../pages/textMgt/index.jsx";
// import dictMgt from "../pages/dictMgt/index.jsx";
import Login from "../pages/login/index.jsx";
// import App from "../App";
import history from "../utils/history";
import Layout from "../pages/layout/Index";
import AuthRouter from "../components/auth/AuthRouter";

export default class ERouter extends React.Component {
  render(h) {
    return (
      <Router history={history}>
        <Switch>
          <Route exact path="/login" component={Login}></Route>
          {/* <Redirect exact from="/" to="/login" />  这个跳转的动作在下方的权限路由中做了处理如果未登陆会自动跳转到login*/}
          <AuthRouter path="/" component={Layout} />
          {/* 这个layout是初始化页面的架构 当做参数传递 props*/}
          {/* 路由权限组件 */}
        </Switch>
      </Router>
    );
  }
}
