

/**
 * @author sulivancc@gmail.com
 */

//这一部分是应用渲染侧边菜单的配置文件
export const menus = [
  //三个页面
  {
    path: "/dashboard",
    title: "首页",
    icon: "home"
  },
  {
    path: "/imgMgt",
    title: "图片管理",
    icon: "camera"
  },
  {
    path: "/textMgt",
    title: "文字管理",
    icon: "form"
  },
  {
    title: "字典项管理",
    path: "/dictMgt",
    icon: "book"
    // children: [ //如添加子菜单
    //     {
    //         path: '/dictMgt/basic',
    //         title: '基本表单'
    //     },
    //     {
    //         path: '/dictMgt/editor',
    //         title: '富文本'
    //     },
    //     {
    //         path: '/dictMgt/markdown',
    //         title: 'MarkDown'
    //     }
    // ]
  }
];
