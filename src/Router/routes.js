import imgMgt from "../pages/imgMgt/index";
import textMgt from "../pages/textMgt/index";
import dictMgt from "../pages/dictMgt/index";
import Error404 from "@/pages/error/Error404";

export const routes = [
  { path: "/imgMgt", component: imgMgt },
  { path: "/textMgt", component: textMgt },
  { path: "/dictMgt", component: dictMgt },
  { path: "/error/404", component: Error404 }
];
