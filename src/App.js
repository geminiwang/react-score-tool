import React, { Component } from "react";
import { Provider } from "react-redux";
// import { Row, Col } from "antd";
// import Header from "./components/Header/index.jsx";
// import Nav from "./components/Nav/index.jsx";
import Router from "./Router/router";
import store from "./store/store";
class App extends Component {
  render(props) {
    return (
      // <Row className="base_warp">
      //   <Header />
      //   <Col span={3} className="nav_warp">
      //     {/* 侧面栏 */}
      //     <Nav></Nav>
      //   </Col>
      //   <Col span={21} className="main_warp">
      //     {this.props.children}
      //   </Col>
      //   {/* Footer */}
      // </Row>
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
